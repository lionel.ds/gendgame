<?php

session_start();

if(isset($_GET['q']) && is_numeric($_GET['q']) && $_GET['q'] <= 10 && $_GET['q']>0){


    if($_GET['q'] == 1){
        $_SESSION['questions'] = "";
        $_SESSION['questions'] = ["init"];

        $_SESSION['answers'] = "";
        $_SESSION['answers'] = ["init"];
    }

    if(!empty($_POST)){
        array_push($_SESSION['answers'], $_POST['answer']);
    }

    $dirList = scandir("./imgs/grades");

    function randImg($list){
        $rand = rand(2, count($list)-1);
        return $list[$rand];
    }

    function answer($randImg){
        return str_replace(array("_", "-", ".png"), array(" ", "'", ""), $randImg);
    }
    $img = randImg($dirList);

    array_push($_SESSION['questions'], answer($img));

    $answers = [
        answer($img),
        answer(randImg($dirList)),
        answer(randImg($dirList)),
        answer(randImg($dirList))
    ];
    shuffle($answers);
}
?>

<! DOCTYPE HTML >
<html>
    <head>
        <title>GendGame</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <link rel="stylesheet" href="./styles/style.css">
    </head>

    <body>
        <div class="container-fluid">
            <header><h1>GendGame</h1></header>
            <div class="content">

                <?php
                    if(!empty($_GET['q']) && is_numeric($_GET['q']) && $_GET['q'] <= 10 && $_GET['q']>0){
                ?>
                    <div class="question">
                        <form action="?q=<?= $_GET['q']+1 ?>" method="POST" name="form-answer">
                            <center>
                                <img src="./imgs/grades/<?= $img; ?>" alt="Grade">
                                <br />
                                <?php
                                    $i = 1;
                                    foreach($answers as $answer){
                                ?>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="answer" id="rep_<?= $i ?>" value="<?= $answer ?>">
                                    <label class="form-check-label" for="rep_<?= $i ?>"><?= $answer ?></label>
                                </div>
                                <?php
                                        $i++;
                                    }
                                ?>
                                <br />
                                <button type="submit" class="btn btn-primary">Envoyer</button>
                            </center>
                        </form>
                    </div>
                <?php
                    } elseif(!empty($_GET['q']) && is_numeric($_GET['q']) && $_GET['q'] == 11){
                        if(!empty($_POST)){
                            array_push($_SESSION['answers'], $_POST['answer']);
                        }
                ?>
                    <h1>Résultat !</h1>
                    <a href="?q=1"><button class="btn btn-primary">Recommencer !</button></a>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Img</th>
                                <th scope="col">SystAnswer</th>
                                <th scope="col">UsrAnswer</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            for($i = 1; $i < sizeof($_SESSION['questions']); $i++){
                        ?>
                            <tr <?= $_SESSION['questions'][$i] == $_SESSION['answers'][$i] ? "style=\"background-color: #28A745;\"" : "style=\"background-color: #DC3545;\"" ?>>
                                <th scope="row"><?= $i ?></th>
                                <td><img src="./imgs/grades/<?= str_replace(array(" ", "'"), array("_", "-"), $_SESSION['questions'][$i]) ?>.png"/></td>
                                <td><?= $_SESSION['questions'][$i] ?></td>
                                <td><?= $_SESSION['answers'][$i] ?></td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tbody>
                        </table>
                <?php
                    } else{
                ?>
                <a href="logout.php"><button class="btn btn-success btn-lg">Commencer !</button></a>
                <?php
                    }
                ?>

            </div>
        </div>
    </body>
</html>
