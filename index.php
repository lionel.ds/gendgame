<?php

session_start();

if(!empty($_GET['page']) && $_GET['page'] == 'grade'){

  $_SESSION['questions'] = "";
  $_SESSION['questions'] = ["init"];

  $_SESSION['answers'] = "";
  $_SESSION['answers'] = ["init"];

    if(!empty($_POST)){
        array_push($_SESSION['answers'], $_POST['answer']);
    }

    $dirList = scandir("./imgs/grades");

    function randImg($list){
        $rand = rand(2, count($list)-1);
        return $list[$rand];
    }

    function answer($randImg){
        return str_replace(array("_", "-", ".png"), array(" ", "'", ""), $randImg);
    }
}

if (!empty($_GET['page']) && $_GET['page'] == 'alphabet') {


  $_SESSION['questions'] = "";
  $_SESSION['questions'] = ["init"];

  $_SESSION['answers'] = "";
  $_SESSION['answers'] = ["init"];

  if(!empty($_POST)){
      array_push($_SESSION['answers'], $_POST['answer']);
  }

  function randLetter(){
    $ABC = array(
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
      'H',
      'I',
      'J',
      'K',
      'L',
      'M',
      'N',
      'O',
      'P',
      'Q',
      'R',
      'S',
      'T',
      'U',
      'V',
      'W',
      'X',
      'Y',
      'Z'
    );

    $randABC = rand(0, 25);
    return $ABC[$randABC];
  }

}
?>

<!DOCTYPE HTML >
<html>
    <head>
        <title>GendGame</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <link rel="stylesheet" href="./styles/style.css">
    </head>

    <body>
        <div class="container-fluid">
            <header>
              <a href="." style="color: #FFFFFF;"><h1>GendGame</h1></a>
              <span>Design et développement réalisé par <a href="http://www.twitter.com/Lionel76000">Lionel</a>.</span>
            </header>
            <div class="content">
              <?php
                  if(!empty($_GET['page']) && $_GET['page'] == 'grade'){
              ?>
              <!-- DÉBUT PARTIE GRADE -->
              <div class="question">
                <form action="?page=gresults" method="POST" name="form-answer">
                  <?php
                    $c = 1;
                    for($i=1; $i < 11; $i++){
                      $img = randImg($dirList);

                      array_push($_SESSION['questions'], answer($img));

                      $answers = [
                        answer($img),
                        answer(randImg($dirList)),
                        answer(randImg($dirList)),
                        answer(randImg($dirList))
                      ];
                      shuffle($answers);
                  ?>
                  <div class="question-content" id="q_<?= $i ?>">

                    <img src="./imgs/grades/<?= $img; ?>" alt="Grade">
                    <br />
                    <?php
                      foreach($answers as $answer){
                    ?>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="answer_<?= $i ?>" id="rep_<?= $c ?>" value="<?= $answer ?>">
                      <label class="form-check-label" for="rep_<?= $c ?>"><?= $answer ?></label>
                    </div>
                    <?php
                        $c++;
                      }
                      if($i == 10){
                    ?>
                    <br />
                    <button id="send" type="submit" class="btn btn-primary">Envoyer</button>
                    <?php
                        } else{
                    ?>
                    <br />
                    <button class="btn btn-primary next">Envoyer</button>
                    <?php
                        }
                    ?>
                  </div>
                  <?php
                    }
                  ?>
                  </form>
                </div>
                <!-- FIN PARTIE GRADE -->
                <?php
              } elseif(!empty($_GET['page']) && $_GET['page'] == 'gresults'){
                ?>
                <!-- DÉBUT PARTIE RÉSULTAT DES GRADES -->
                <h1>Résultat !</h1>
                <a href="?page=grade"><button class="btn btn-success">Recommencer</button></a>
                <a href="."><button class="btn btn-primary">Accueil</button></a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Img</th>
                            <th scope="col">SystAnswer</th>
                            <th scope="col">UsrAnswer</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        for($i = 1; $i < sizeof($_SESSION['questions']); $i++){
                    ?>
                        <tr <?= $_SESSION['questions'][$i] == $_POST['answer_' . $i] ? "style=\"background-color: #28A745;\"" : "style=\"background-color: #DC3545;\"" ?>>
                            <th scope="row"><?= $i ?></th>
                            <td><img src="./imgs/grades/<?= str_replace(array(" ", "'"), array("_", "-"), $_SESSION['questions'][$i]) ?>.png"/></td>
                            <td><?= $_SESSION['questions'][$i] ?></td>
                            <td><?= $_POST['answer_' . $i] ?></td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                  </table>
                  <!-- FIN PARTIE RÉSULTAT DES GRADES -->
                <?php
              } elseif(!empty($_GET['page']) && $_GET['page'] == 'alphabet'){
                ?>
                <!-- DÉBUT PARTIE ALPHABET -->
                <div class="question">
                  <form action="?page=aresults" method="POST" name="form-answer">
                    <?php
                      $c = 1;
                      for($i=1; $i < 11; $i++){
                        $letter = randLetter();

                        array_push($_SESSION['questions'], $letter);

                    ?>
                    <div class="question-content" id="q_<?= $i ?>">

                      <h1><?=$letter?></h1>
                      <br />
                      <input type="text" id="answer_<?= $i ?>" name="answer_<?= $i ?>"/>
                      <?php
                        if($i == 10){
                      ?>
                      <br />
                      <button id="send" type="submit" class="btn btn-primary">Envoyer</button>
                      <?php
                          } else{
                      ?>
                      <br />
                      <button class="btn btn-primary next">Envoyer</button>
                      <?php
                          }
                      ?>
                    </div>
                    <?php
                      }
                    ?>
                    </form>
                  </div>
                  <!-- FIN PARTIE ALPHABET -->
                <?php
                //DÉBUT PARTIE RÉSULTAT ALPHABET
              } elseif(!empty($_GET['page']) && $_GET['page'] == 'aresults'){
                $alphabet = array(
                  'A' => 'alpha',
                  'B' => 'bravo',
                  'C' => 'charlie',
                  'D' => 'delta',
                  'E' => 'echo',
                  'F' => 'fox-trot',
                  'G' => 'golf',
                  'H' => 'hotel',
                  'I' => 'india',
                  'J' => 'juliett',
                  'K' => 'kilo',
                  'L' => 'lima',
                  'M' => 'mike',
                  'N' => 'november',
                  'O' => 'oscar',
                  'P' => 'papa',
                  'Q' => 'quebec',
                  'R' => 'romeo',
                  'S' => 'sierra',
                  'T' => 'tango',
                  'U' => 'uniform',
                  'V' => 'victor',
                  'W' => 'wisky',
                  'X' => 'x-ray',
                  'Y' => 'yankee',
                  'Z' => 'zoulou'
                );
                ?>
                <h1>Résultat !</h1>
                <a href="?page=alphabet"><button class="btn btn-success">Recommencer</button></a>
                <a href="."><button class="btn btn-primary">Accueil</button></a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Letter</th>
                            <th scope="col">SystAnswer</th>
                            <th scope="col">UsrAnswer</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        for($i = 1; $i < sizeof($_SESSION['questions']); $i++){
                    ?>
                        <tr <?= $alphabet[$_SESSION['questions'][$i]] == strtolower($_POST['answer_' . $i]) ? "style=\"background-color: #28A745;\"" : "style=\"background-color: #DC3545;\"" ?>>
                            <th scope="row"><?= $i ?></th>
                            <td><?= $_SESSION['questions'][$i] ?></td>
                            <td><?= $alphabet[$_SESSION['questions'][$i]] ?></td>
                            <td><?= $_POST['answer_' . $i] ?></td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                  </table>
                  <!-- FIN PARTIE RÉSULTAT ALPHABET -->
                <?php
              } elseif(!empty($_GET['page']) && $_GET['page'] == 'alearn'){
                ?>
                <table class="table" id="alearnTab">
                    <thead>
                        <tr>
                            <th scope="col">Lettre</th>
                            <th scope="col">Alphabet phonétique</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>A</td>
                        <td>Alpha</td>
                      </tr>

                      <tr>
                        <td>B</td>
                        <td>Bravo</td>
                      </tr>
                      <tr>
                        <td>C</td>
                        <td>Charlie</td>
                      </tr>
                      <tr>
                        <td>D</td>
                        <td>Delta</td>
                      </tr>
                      <tr>
                        <td>E</td>
                        <td>Echo</td>
                      </tr>
                      <tr>
                        <td>F</td>
                        <td>Fox-trot</td>
                      </tr>
                      <tr>
                        <td>G</td>
                        <td>Golf</td>
                      </tr>
                      <tr>
                        <td>H</td>
                        <td>Hotel</td>
                      </tr>
                      <tr>
                        <td>I</td>
                        <td>India</td>
                      </tr>
                      <tr>
                        <td>J</td>
                        <td>Juliett</td>
                      </tr>
                      <tr>
                        <td>K</td>
                        <td>Kilo</td>
                      </tr>
                      <tr>
                        <td>L</td>
                        <td>Lima</td>
                      </tr>
                      <tr>
                        <td>M</td>
                        <td>Mike</td>
                      </tr>
                      <tr>
                        <td>N</td>
                        <td>November</td>
                      </tr>
                      <tr>
                        <td>O</td>
                        <td>Oscar</td>
                      </tr>
                      <tr>
                        <td>P</td>
                        <td>Papa</td>
                      </tr>
                      <tr>
                        <td>Q</td>
                        <td>Quebec</td>
                      </tr>
                      <tr>
                        <td>R</td>
                        <td>Romeo</td>
                      </tr>
                      <tr>
                        <td>S</td>
                        <td>Sierra</td>
                      </tr>
                      <tr>
                        <td>T</td>
                        <td>Tango</td>
                      </tr>
                      <tr>
                        <td>U</td>
                        <td>Uniform</td>
                      </tr>
                      <tr>
                        <td>V</td>
                        <td>Victor</td>
                      </tr>
                      <tr>
                        <td>W</td>
                        <td>Wisky</td>
                      </tr>
                      <tr>
                        <td>X</td>
                        <td>X-ray</td>
                      </tr>
                      <tr>
                        <td>Y</td>
                        <td>Yankee</td>
                      </tr>
                      <tr>
                        <td>Z</td>
                        <td>Zoulou</td>
                      </tr>
                    </tbody>
                  </table>
                <?php
              } else{
                ?>
                <div class="second-content">
                  <a href="?page=grade"><button id ="play" class="play btn btn-success">Maîtriser les grades</button></a>
                  <a href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=14&ved=0ahUKEwj6uc_azJ3cAhVSzYUKHccAD5UQFghHMA0&url=https%3A%2F%2Fwww.gendarmerie.interieur.gouv.fr%2Fcegn%2Fcontent%2Fdownload%2F1368%2F15280%2Ffile%2FLes%2520grades%2520dans%2520la%2520gendarmerie.pdf&usg=AOvVaw0f_wYszkIPDXhJBt3-_Pdu" target="_blank"><button class="learn btn btn-primary">Apprendre les grades</button></a>
                  <br/>
                  <a href="?page=alphabet"><button class="play btn btn-success">Maîtriser l'alphabet</button></a>
                  <a href="?page=alearn"><button class="learn btn btn-primary">Apprendre l'alphabet</button></a>
                </div>
                <?php
                  }
                ?>
            </div>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="./js/app.js"></script>
    </body>
</html>
