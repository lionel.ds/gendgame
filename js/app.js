$('.next').click(function(e){
  e.preventDefault();
  var next_q = "q_";
  var question_number = e.target.parentElement.id[2];
  $(e.target.parentElement).fadeOut("slow");
  question_number = parseInt(question_number) + 1;
  next_q += question_number.toString();
  $('#' + next_q).delay(600).fadeIn("slow");
  console.log($("#answer_" + question_number));
});